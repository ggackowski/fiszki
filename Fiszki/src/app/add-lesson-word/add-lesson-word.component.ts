import { Component, OnInit, Input } from '@angular/core';
import Word from '../../models/word-model';

@Component({
  selector: 'app-add-lesson-word',
  templateUrl: './add-lesson-word.component.html',
  styleUrls: ['./add-lesson-word.component.css']
})
export class AddLessonWordComponent implements OnInit {


  @Input()
  word: Word;
  constructor() { }

  ngOnInit() {
  }

}
