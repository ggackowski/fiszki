import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLessonWordComponent } from './add-lesson-word.component';

describe('AddLessonWordComponent', () => {
  let component: AddLessonWordComponent;
  let fixture: ComponentFixture<AddLessonWordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLessonWordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLessonWordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
