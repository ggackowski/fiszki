import { Injectable } from '@angular/core';
import Lesson from '../models/lesson-model';
import Word from '../models/word-model';
import {sampleLessons} from '../models/sample-lessons';

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

@Injectable({
  providedIn: 'root'
})
export class LearningSessionService {
  lesson: Lesson;
  lessons: Array<Lesson>;
  activeLesson: Lesson;
  points: Array<number>;
  actualQuestion: number;
  finished: boolean;
  good: number;

  constructor() {
    this.lessons = sampleLessons();
  }

  addLesson = (words: Array<Word>, name: string): void => {
    let newLesson: Lesson = new Lesson();
    newLesson.words = words;
    newLesson.name = name;
    newLesson.id = this.lessons[this.lessons.length - 1].id + 1;
    this.lessons.push(newLesson);
    console.log(newLesson.id);
    console.log(this.lessons.length);
  }

  getLessonByName = (name: string): Lesson => {
    return this.lessons.find(les => les.name === name);
  }

  getLessonById = (id: number): Lesson => {
    console.log(this.lessons);
    console.log(this.lessons[1].id);
    console.log(id);
    return this.lessons.find(les => les.id === id);
  }

  getLessons = (): Array<Lesson> => {
    return this.lessons;
  }


  setActiveLessonById = (id: number): void => {
    console.log(this.getLessonById(id));
    this.setActiveLesson(this.getLessonById(id));
  }

  setActiveLessonByName = (name: string): void => {
    this.setActiveLesson(this.getLessonByName(name));
  }

  setActiveLesson = (lesson: Lesson): void => {
    this.good = 0;
    this.activeLesson = lesson;
    console.log('aktywna lekcja: ' + this.activeLesson.name);
    this.points = new Array<number>(this.activeLesson.words.length);
    console.log('tablica punktow rozmiar: ' + this.points.length);
    this.actualQuestion = getRandomInt(0, this.points.length - 1);
    console.log('aktywne pytanie: ' + this.actualQuestion);
    console.log('jakie pytanie: ' + this.activeLesson.words[this.actualQuestion].targetLangWord);
    console.log('jaka odpowiedz: ' + this.activeLesson.words[this.actualQuestion].meaningWord);
    this.finished = false;
    for (let i = 0; i < this.points.length; ++i) {
      this.points[i] = 0;
    }
    for (const point of this.points) {
      console.log( point);
    }

  }

  isFinished = (): boolean => {
    return this.finished;
  }

  getQuestion = (): string => {
    console.log('podaje question: ' + this.activeLesson.words[this.actualQuestion].targetLangWord);
    return this.activeLesson.words[this.actualQuestion].targetLangWord;
  }

  checkAnswer = (answer: string): boolean => {
    console.log('jaka odpowiedz: ' + answer);
    console.log('jaka byla oczekiwana: ' + this.activeLesson.words[this.actualQuestion].meaningWord);
    if (answer === this.activeLesson.words[this.actualQuestion].meaningWord) {
      if (this.points[this.actualQuestion] === 0) {
        this.good++;
      }
      console.log('poprawna odpowiedz ' + answer);
      this.points[this.actualQuestion] = 1;
      console.log('points [' + this.actualQuestion + '] = 1');
      let isFinished = true;
      for (const point of this.points) {
        if (point !== 1) {
          isFinished = false;
        }
      }
      if (isFinished === true) {
        this.finished = true;
        return true;
      } else {

        let random: number = getRandomInt(0, this.points.length - 1);

        while (this.points[random] === 1) {
          random = getRandomInt(0, this.points.length - 1);
        }
        this.actualQuestion = random;
        return true;
      }
    } else {
      this.points[this.actualQuestion] = 0.1;
      /*let random: number = getRandomInt(0, this.points.length);
      while (this.points[random] === 1) {
        random = getRandomInt(0, this.points.length);
      }
      this.actualQuestion = random;
      */

      return false;
    }
  }

  getScore = (): number => {
    if (this.finished) {
      return 100 * this.good / this.activeLesson.words.length;
    } else {
      return -1;
    }
  }

}
