import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLessonInputComponent } from './add-lesson-input.component';

describe('AddLessonInputComponent', () => {
  let component: AddLessonInputComponent;
  let fixture: ComponentFixture<AddLessonInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLessonInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLessonInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
