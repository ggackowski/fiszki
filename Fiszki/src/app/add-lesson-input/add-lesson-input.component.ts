import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import Word from '../../models/word-model';

@Component({
  selector: 'app-add-lesson-input',
  templateUrl: './add-lesson-input.component.html',
  styleUrls: ['./add-lesson-input.component.css']
})
export class AddLessonInputComponent implements OnInit {

  @Output()
  wordChange = new EventEmitter<Word>();

  word: Word;

  constructor() {
    this.word = new Word();
  }

  ngOnInit() {
  }

  addWord = (): void => {
    let newWord: Word = new Word();
    newWord.meaningWord = this.word.meaningWord;
    newWord.targetLangWord = this.word.targetLangWord;
    this.wordChange.emit(newWord);
    this.word.meaningWord = '';
    this.word.targetLangWord = '';
  }

}
