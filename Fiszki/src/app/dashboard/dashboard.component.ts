import { Component, OnInit } from '@angular/core';
import {LearningSessionService} from '../learning-session.service';
import Lesson from '../../models/lesson-model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  lessons: Array<Lesson>;
  constructor(private learningService: LearningSessionService) {

    this.lessons = this.learningService.getLessons();
  }

  ngOnInit() {
  }

}
