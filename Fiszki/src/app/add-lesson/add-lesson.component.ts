import { Component, OnInit } from '@angular/core';
import Word from '../../models/word-model';
import { LearningSessionService } from '../learning-session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-lesson',
  templateUrl: './add-lesson.component.html',
  styleUrls: ['./add-lesson.component.css']
})
export class AddLessonComponent implements OnInit {

  words: Array<Word>;
  name: string;

  constructor(private learningSessionService: LearningSessionService,
    private router: Router) {
    this.words = new Array<Word>();
  }

  ngOnInit() {
  }

  addWord = (event): void => {
    this.words.push(event);
  }

  addLesson = (): void => {
    this.learningSessionService.addLesson(this.words, this.name);
    this.router.navigateByUrl('/');
  }

}
