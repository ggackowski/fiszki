import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  constructor(private router: Router, private authService: AuthService) { }

  login() {
    this.authService.login({email: this.email, password: this.password}).then( u => {
      if (u) 
        this.router.navigate(['/']);
    })
  }

  ngOnInit() {
  }

}
