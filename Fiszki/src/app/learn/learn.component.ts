import { Component, OnInit } from '@angular/core';
import {LearningSessionService} from '../learning-session.service';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-learn',
  templateUrl: './learn.component.html',
  styleUrls: ['./learn.component.css']
})
export class LearnComponent implements OnInit {


  word: string;
  answer: string;

  isGoodAnswer: boolean;
  isFinished: boolean;
  doneCount: number;

  constructor(
    private route: ActivatedRoute,
    private learningService: LearningSessionService
    ) {

  }

  setAnswer(): void {
    console.log(this.answer);
    console.log(this.learningService.actualQuestion);
    this.isGoodAnswer = this.learningService.checkAnswer(this.answer) ? true : false;
    if (this.isGoodAnswer) {
      if (this.learningService.isFinished()) {
        this.isFinished = true;
      } else {
        this.word = this.learningService.getQuestion();
        this.answer = '';
        this.doneCount++;
      }
    }
  }

  ngOnInit() {
    const id = Number.parseInt(this.route.snapshot.paramMap.get('id'));
    console.log(id);
    this.isGoodAnswer = undefined;
    this.learningService.setActiveLessonById(id);
    console.log('bla')
    this.word = this.learningService.getQuestion();
    this.isFinished = false;
    this.doneCount = 0;

  }

}
