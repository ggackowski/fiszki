import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LearnComponent } from './learn/learn.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule, Routes } from '@angular/router';
import {MatToolbarModule} from '@angular/material/toolbar';
import { AddLessonComponent } from './add-lesson/add-lesson.component';
import { AddLessonInputComponent } from './add-lesson-input/add-lesson-input.component';
import { AddLessonWordComponent } from './add-lesson-word/add-lesson-word.component';
import {MatDividerModule} from '@angular/material/divider';
import { LoginComponent } from './login/login.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from './environment';
const appRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: { title: 'Dashboard' }
  },
  {
    path: 'lesson/:id',
    component: LearnComponent,
    data: { title: 'Learn' }
  },
  {
    path: 'addlesson',
    component: AddLessonComponent,
    data: { title: 'Create new lesson'}
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Log in'}
  },
  { path: '',
  redirectTo: '/dashboard',
  pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LearnComponent,
    DashboardComponent,
    NavbarComponent,
    FooterComponent,
    AddLessonComponent,
    AddLessonInputComponent,
    AddLessonWordComponent,
    LoginComponent,

  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatToolbarModule,
    MatDividerModule,
    AngularFireAuthModule,
    
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
