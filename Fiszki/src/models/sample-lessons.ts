import Lesson from './lesson-model';

export function sampleLessons(): Array<Lesson> {
  return ([
    {
    id: 0,
    words: [
      {targetLangWord: 'pear', meaningWord: 'gruszka'},
      {targetLangWord: 'eye', meaningWord: 'oko'},
      {targetLangWord: 'surname', meaningWord: 'nazwisko'}
    ],
    name: 'Sample Lesson one'
  },
  {
    id: 1,
    words: [
      {targetLangWord: 'number', meaningWord: 'numer'},
      {targetLangWord: 'father', meaningWord: 'ojciec'}
    ],
    name: 'Sample Lesson two'
  }
  ]
  );
}
