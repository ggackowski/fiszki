import Word from './word-model';

class Lesson {
  words: Array<Word>;
  name: string;
  id: number;
  constructor() {
    this.words = new Array<Word>();
  }
}

export default Lesson;
